
## Project setup

npm install


### Compiles and hot-reloads for development

npm run serve


### Compiles and minifies for production

npm run build


### End-to-end tests

npm run test:e2e

