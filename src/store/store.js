import Vue from 'vue'
import Vuex from 'vuex'

// modules
//import auth from './modules/auth';
import settings from './modules/settings';
import seminare from './modules/seminare';
import menu from './modules/menu';




Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        //auth,
        seminare,
        settings,
        menu,
        
    }
})
