import svbTheme from "Themes/svbTheme";

// languages
export const languages = [
   {
      name: "Deutsch",
      icon: "de",
      locale: "de"
   },
]

// sidebar filters
export const sidebarFilters = [
   {
      id: 1,
      class: 'sidebar-overlay-light',
      name: 'message.light'
   },
]

// router animations
export const routerAnimations = [
   'fadeIn',
   'slideInUp',
   'slideInRight',
]

// themes
export const themes = [
   {
      id: 1,
      bgColor: "bg-primary",
      theme: svbTheme
   },
]

// header filter
export const headerFilters = [
   {
      id: 1,
      class: "primary"
   },
]