/**
 * Seminare Module
 */
import { cart } from "./data";
//import { seminareData } from './seminare_data.js';

const state = {
   cart,
   ecommerceSidebarFilter: false
}

// getters
const getters = {
   cart: state => {
      return state.cart;
   },
   ecommerceSidebarFilter: state => {
      return state.ecommerceSidebarFilter;
   }
}


// actions
const actions = {
   addProductToCart(context, payload) {
      context.commit('onAddProductToCart', payload);
   },
   onDeleteProductFromCart(context, payload) {
      context.commit("onDeleteProductFromCart", payload);
   },
   changeQuantityHandler(context, payload) {
      context.commit('onChangeQuantityHandler', payload);
   },
   toggleEcommerceSidebarFilter(context, payload) {
      context.commit('toggleEcommerceSidebarFilterHandler', payload);
   }
}

// mutations
const mutations = {
   onAddProductToCart(state, payload) {
      let newProduct = {
         id: payload.objectID,
         name: payload.titel,
         price: payload.preis1,
         quantity: 1,
         total: payload.preis1
      }
      state.cart.push(newProduct);
   },
   onDeleteProductFromCart(state, payload) {
      let index = state.cart.indexOf(payload);
      state.cart.splice(index, 1);
   },
   onChangeQuantityHandler(state, payload) {
      let quantity = payload.e.target.value,
         cartItem = payload.cartItem,
         indexOfItem = state.cart.indexOf(cartItem)
      state.cart[indexOfItem].total = cartItem.price * quantity;
      state.cart[indexOfItem].quantity = quantity;
   },
   toggleEcommerceSidebarFilterHandler(state, payload) {
      state.ecommerceSidebarFilter = payload;
   }
}

export default {
   state,
   getters,
   actions,
   mutations
}