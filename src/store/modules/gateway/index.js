/**
 * Payment Gateway
 */
import { cart, creditCard } from "./data";

const state = {
   cart,
   creditCard,
   ecommerceSidebarFilter: false
}

// getters
const getters = {
   cart: state => {
      return state.cart;
   },
   creditCard: state => {
      return state.creditCard;
   },
   ecommerceSidebarFilter: state => {
      return state.ecommerceSidebarFilter;
   }
}

// actions
const actions = {
   onDeleteCard(context, payload) {
      context.commit('onDeleteCard', payload);
   },
   addNewCard(context, payload) {
      context.commit('addNewCard', payload);
   },
   toggleEcommerceSidebarFilter(context, payload) {
      context.commit('toggleEcommerceSidebarFilterHandler', payload);
   }
}

// mutations
const mutations = {
   onDeleteCard(state, payload) {
      let index = state.creditCard.indexOf(payload);
      state.creditCard.splice(index, 1);
   },
   addNewCard(state, payload) {
      let cardNumber = payload.last4Digit.substr(0, 12);
      let last4Digits = payload.last4Digit.substr(12);
      let first12digits = payload.last4Digit.replace(cardNumber, '************');
      let newCardNumber = payload.last4Digit.replace(cardNumber, '***********'); + last4Digits;
      payload.last4Digit = newCardNumber;
      state.creditCard.push(payload);
   },
   toggleEcommerceSidebarFilterHandler(state, payload) {
      state.ecommerceSidebarFilter = payload;
   }
}

export default {
   state,
   getters,
   actions,
   mutations
}
