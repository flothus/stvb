export const cart = [

]

export const creditCard = [
   {
      id: 0,
      last4Digit: '************8549',
      cvv: 465,
      cardHolderName: 'Max Muserfrau'
   },
   {
      id: 1,
      last4Digit: '************6526',
      cvv: 123,
      cardHolderName: ' Max Mustermann'
   },
   {
      id: 2,
      last4Digit: '************9645',
      cvv: 789,
      cardHolderName: 'F F'
   }
]