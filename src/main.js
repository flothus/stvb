/**
 * App Entry File
 * StVB-SACHSEN
 * 
 */
import 'babel-polyfill';
import Vue from 'vue'
import Vuetify from 'vuetify'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import Nprogress from 'nprogress'						// Progress Bar
import VueI18n from 'vue-i18n'					
import InstantSearch from 'vue-instantsearch'			// Algolia Instant Search

// global components
import GlobalComponents from './globalComponents'

// app.vue
import App from './App'

// router
import router from './router'

// store
import { store } from './store/store';

// include script file
import './lib/appScript'

// include css files
import './lib/appCss'

// include translations
import messages from './lang';



// navigation guards before each
router.beforeEach((to, from, next) => {
	Nprogress.start()
	if (to.matched.some(record => record.meta.requiresAuth)) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		
		if (localStorage.getItem('user') === null) {
			/*
			next({
				path: '/session/login',
				query: { redirect: to.fullPath }
			})
			*/
		} else {
			next()
		}
		
	} else {
		next() // make sure to always call next()!
	}
})


// navigation guard after each
router.afterEach((to, from) => {
	Nprogress.done()
	setTimeout(() => {
		const contentWrapper = document.querySelector(".v-content__wrap");
		if(contentWrapper !== null){
			contentWrapper.scrollTop = 0;
		}
		const boxedLayout = document.querySelector('.app-boxed-layout .app-content');
		if(boxedLayout !==  null){
			boxedLayout.scrollTop = 0;
		}
	}, 200);
})



// plugins
Vue.use(Vuetify, {
	theme: store.getters.selectedTheme.theme
});
Vue.use(InstantSearch);
Vue.use(VueI18n)
Vue.use(Notifications, { velocity })
Vue.use(GlobalComponents);

// Create VueI18n instance with options
const i18n = new VueI18n({
	locale: store.getters.selectedLocale.locale, // set locale
	messages, // set locale messages
	silentTranslationWarn: true
})

/* eslint-disable no-new */
new Vue({
	store,
	i18n,
	router,
	render: h => h(App),
	components: { App }
}).$mount('#app')
