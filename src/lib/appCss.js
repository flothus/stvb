/* ======= All App File Include Here ======= */
// Vuetify Css
import 'vuetify/dist/vuetify.css'

// nprogress
import 'nprogress/nprogress.css'

// Icon Files
import '../assets/themify-icons/themify-icons.css'

// leaflet map css
import 'leaflet/dist/leaflet.css'

// animate css
import '../assets/animate.css';

// Global Scss File
import '../assets/scss/_style.scss'
