/**
 * App Config File
 */
export default {
	appLogo: '/static/svg/logos/logo_stbverband-sachsen.svg',                   // App Logo
	darkLogo: '/static/svg/logos/logo_stbverband-sachsen.svg',   				// dark logo
	appLogo2: '/static/img/session.png',                                   	 	// Login & Signup Page
	brand: 'StVB',                                        			        	// Brand Name
	copyrightText: 'FRG',					                  	  				// Copyright Text
	//enableUserTour: process.env.NODE_ENV === 'production' ? true : false,  	// Enable User Tour
	ApiId: '',																	
	ApiKey: ''																	
}
