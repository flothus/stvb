const demo = [
    {
      id: 1,
      attributes: {
        type: "praktiker",
        place: "leipzig",
        month: "jan",
        name: ["helmut schmidt", "Ludwig erhard"]
      }
    },
    {
      id: 2,
      attributes: {
        type: "halbtags",
        place: "leipzig",
        month: "may",
        name: ["helmut schmidt"]
      }
    },
    {
      id: 3,
      attributes: {
        type: "mitarbeiter",
        place: "bautzen",
        month: "may",
        name: ["konrad adenauer", "willy brandt"]
      }
    },
    {
      id: 4,
      attributes: {
        type: "praktiker",
        place: "chemnitz",
        month: "aug",
        name: ["helmut schmidt"]
      }
    },
    {
      id: 5,
      attributes: {
        type: "dialog",
        place: "chemnitz",
        month: "aug",
        name: ["konrad adenauer"]
      }
    },
    {
      id: 6,
      attributes: {
        type: "praktiker",
        place: "chemnitz",
        month: "oct",
        name: ["Ludwig erhard"]
      }
    },
    {
      id: 7,
      attributes: {
        type: "ganztags",
        place: "dresden",
        month: "dec",
        name: ["willy brandt"]
      }
    }
]

export default demo;