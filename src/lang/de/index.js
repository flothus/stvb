import seminare from './seminare.js';
import allgemein from './basic.js';

export default Object.assign( 
  seminare,
  allgemein
) 
