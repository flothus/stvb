import de from './de';
import en from './en';

export default {
    de: {
        message: de
    },
    en: {
        message: en
    },
}