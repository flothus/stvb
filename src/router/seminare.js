import defaultLayout from 'Container/defaultLayout'


//Seminare Widgets
const Seminare = () => import('Views/seminare/Seminare');
const Isotope = () => import('Views/seminare/Isotope');
const Cart = () => import('Views/seminare/Cart');
const Checkout = () => import('Views/seminare/Checkout');
//const PaymentGateway = () => import('Views/seminare/PaymentGateway');
const SeminareDetail = () => import('Views/seminare/SeminareDetail');

const Testing = () => import('Views/seminare/Testing');


export default {
    path: '/',
    component: defaultLayout,
    redirect: '/seminare',
    children: [
        {
            path: '/seminare',
            component: Seminare,
            meta: {
                requiresAuth: false,
                title: 'message.shop',
                breadcrumb: [{
                        breadcrumbInactive: 'Seminare /'
                    },
                    {
                        breadcrumbActive: 'Seminare'
                    }
                ]
            }
        },
        {
            path: '/seminare/seminar/:id',
            component: SeminareDetail,
            meta: {
                requiresAuth: false,
                title: 'message.seminarDetail',
                breadcrumb: [{
                        breadcrumbInactive: 'Seminare /'
                    },
                    {
                        breadcrumbActive: 'Seminare Detail'
                    }
                ]
            }
        },
        {
            path: '/cart',
            component: Cart,
            meta: {
                requiresAuth: false,
                title: 'message.cart',
                breadcrumb: [{
                        breadcrumbInactive: ''
                    },
                    {
                        breadcrumbActive: 'Cart'
                    }
                ]
            }
        },
        {
            path: '/checkout',
            component: Checkout,
            meta: {
                requiresAuth: false,
                title: 'message.checkout',
                breadcrumb: [{
                        breadcrumbInactive: ''
                    },
                    {
                        breadcrumbActive: 'Checkout'
                    }
                ]
            }
        },
        {
            path: '/seminare-isotope',
            component: Isotope,
            meta: {
                requiresAuth: false,
                title: 'message.isotope',
                breadcrumb: [{
                        breadcrumbInactive: ''
                    },
                    {
                        breadcrumbActive: 'Isotope'
                    }
                ]
            }
        },
        {
            path: '/testing',
            component: Testing,
            meta: {
                requiresAuth: false,
                title: Testing,
                breadcrumb: [{
                        breadcrumbInactive: '/'
                    },
                    {
                        breadcrumbActive: 'Testing'
                    }
                ]
            }
        },
    ]
}
